<?php 

	function preparePackages($products, $max_package_weight) 
	{
		// start with an empty package
		$packages = array();
		$current_package = array('items' => array(), 'package_weight' => 0);
		
		foreach($products as $product) 
		{
			// if package weight is greater than user defined
			if($product['weight'] > $max_package_weight) 
			{
				throw new Exception('Product weight exceeds maximum package weight');
			}
			
			// Check if adding this product will exceed the maximum package weight
			if($current_package['package_weight'] + $product['weight'] > $max_package_weight) 
			{
				// Add the current package to the packages array
				$packages[] = $current_package;
				
				// Create a new package with the current product
				$current_package = array('items' => array($product['id']), 'package_weight' => $product['weight']);
			} 
			else 
			{
				// Add the product to the current package
				$current_package['items'][] = $product['id'];
				$current_package['package_weight'] += $product['weight'];
			}
		}
		
		// Add the last package to the packages array
		if(!empty($current_package['items'])) 
		{
			$packages[] = $current_package;
		}
		
		return $packages;
	}
?>

